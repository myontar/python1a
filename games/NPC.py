from games.Actor import Actor


class Satici(Actor):
    urunler = [
        {'isim':'Ekmek', 'fiyat':1, 'tip':1,'deger':10},
        {'isim':'Balık', 'fiyat':1, 'tip':1,'deger':10},
        {'isim':'Et', 'fiyat':1, 'tip':1,'deger':50},
        {'isim':'Su', 'fiyat':1, 'tip':4,'deger':100},
        {'isim':'Kahve', 'fiyat':1, 'tip':3,'deger':50},
        {'isim':'Bitki Çayı', 'fiyat':1, 'tip':2,'deger':10},
    ]

    def etkilesim(self, secenek=0):
        if secenek == 0:
            urunlistesi = []
            urunid = 0
            print('Merhaba, Ben {}\n size nasıl hizmet edebilirim ?'.format(self.isim))
            for urun in self.urunler:
                urunid += 1
                print("{} ({} Altın)".format(
                    urun.get('isim'), urun.get('fiyat')
                ))
            return None
        else:
            if len(self.urunler) >= secenek:
                return self.urunler[secenek-1]


class Demirci(Satici):
    urunler = [
        {'isim':'Balta', 'fiyat':1, 'tip':9,'deger':10},
        {'isim':'Kılıç', 'fiyat':1, 'tip':10,'deger':10},
        {'isim':'Kalkan', 'fiyat':1, 'tip':11,'deger':50},
    ]