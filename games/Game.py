from games.Level import Level
from games.Oyuncu import Oyuncu


class Game:
    basladimi = False
    karakter = None
    level = None

    def __init__(self):
        print('Oyuna Başlamak için isim girin')
        karakter_ismi = input('isminiz :')
        self.karakter = Oyuncu()
        self.karakter.isim = karakter_ismi
        self.level = Level(self.karakter)
        print('Hoş geldin {} '.format(self.karakter.isim))
        self.run()

    def run(self):
        while True:
            print('1 - Haraket et')
            print('2 - Etrafa Bak')
            islem = input('Seçim : ')
            if islem == "2":
                self.level.etrafa_bak()
                print('Birisi ile konuşmak istiyor musun ?')
                evet = input('E/H :')
                if evet.strip().lower() == 'e':
                    kim = input('Kimle ?')
                    self.level.etkilesim(int(kim))




g = Game()
