class Actor:
    can = 100
    enerji = 100
    guc = 1
    zeka = 1
    ceviklik = 1
    kullanilabilir_yetenek_puani = 0
    deneyim = 0
    deneyim_seviyesi = 1
    yasiyormu = True
    cantasi = []
    giydikleri = []
    para = 3
    isim = ""

    def iyilestirme(self, kaccan):
        if self.can + kaccan >= 100:
            self.can = 100
        else:
            self.can += kaccan

    def enerji_doldur(self, kacenerji):
        if self.enerji + kacenerji >= 100:
            self.enerji = 100
        else:
            self.enerji += kacenerji

    def hasar(self, kaccan):
        if self.can - kaccan > 0:
            self.can -= kaccan
        else:
            self.yasiyormu = False
