from games.NPC import Demirci, Satici


class Level:
    oyuncular = []
    yancilar_npc = []
    kotuler = []
    gun = 0
    max_gun = 10
    """
    yon = 0 yukarı
    yon = 1 sol
    yon = 2 sag
    yon = 3 asagi
    """
    ana_karakter_kare = [0, 0]
    harita_boyutu = [40, 40]
    merkez = [0, 0]

    def __init__(self, oyuncu):
        demirci = Demirci()
        demirci.isim = 'Haydar'

        satici = Satici()
        satici.isim = 'Erşan Küneri'

        self.yancilar_npc = [
            {'x': 1, 'y': 0, 'kim': demirci},
            {'x': 0, 'y': 1, 'kim': satici}
        ]

        self.oyuncular.append(oyuncu)

    def etrafa_bak(self):
        id = 0
        for yanci in self.yancilar_npc:
            id += 1
            if (self.ana_karakter_kare[0] - 1 <= yanci.get('x') <= self.ana_karakter_kare[0] + 1) or (
                    self.ana_karakter_kare[1] - 1 <= yanci.get('y') <= self.ana_karakter_kare[1] + 1):
                print('{} - {}'.format(id, yanci.get('kim').isim))

    def ilerle(self, kackare=1, yon=0):
        if yon == 0:
            self.ana_karakter_kare[1] = self.ana_karakter_kare[1] + kackare
        elif yon == 3:
            self.ana_karakter_kare[1] = self.ana_karakter_kare[1] - kackare

        elif yon == 1:
            self.ana_karakter_kare[0] = self.ana_karakter_kare[0] - kackare
        elif yon == 2:
            self.ana_karakter_kare[0] = self.ana_karakter_kare[0] + kackare

    def vurma(self, kim, kime):
        ""

    def etkilesim(self, kim, secim=0):
        yanci = self.yancilar_npc[kim-1]
        yanci.get('kim').etkilesim(secim)



