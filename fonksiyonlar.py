def ekrana_yaz():
    print('Merhaba')
    print('Dunya')

def merhaba_de(isim,soyisim=''):
    print('Merhaba {} {}, ben bir fonksiyonum'.format(isim,soyisim))

merhaba_de('OĞUZHAN')
merhaba_de(soyisim="HÜLAYİM",isim='OĞUZHAN')
merhaba_de('Mustafa','Ahmet')
merhaba_de('Diren','Aydın')
merhaba_de('Muhammet','Kahraman')


def merhaba_hersey(yas,*args,**kwargs):
    print(args)
    print(kwargs)
    print(yas)

merhaba_hersey(yas=12,isim='Doğukan',soyisim='Yuvanç')